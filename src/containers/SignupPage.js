import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter, Redirect } from 'react-router-dom';
import { pushErrorMessage, resetErrorMessage, signupUser } from '../actions';

var _ = require('lodash');

class SignupPage extends Component {
    static propTypes = {
        history: PropTypes.shape({
            push: PropTypes.func.isRequired,
        }),
        isFetching: PropTypes.bool.isRequired,
        isAuthenticated: PropTypes.bool.isRequired,
        signupUser: PropTypes.func.isRequired,
        pushErrorMessage: PropTypes.func.isRequired,
        resetErrorMessage: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        this.state = {
            name: "",
            email: "",
            password: "",
            repeatPassword: "",
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(e) {
        e.preventDefault();

        const { pushErrorMessage, resetErrorMessage, signupUser } = this.props;

        if (this.state.name.length < 3) {
            pushErrorMessage("Name should be at least 3 charatectrs");
            return
        }

        if (this.state.email === "") {
            pushErrorMessage("enter a valid email");
            return
        }

        if (this.state.password.length < 6) {
            pushErrorMessage("password should be at least 6 symbols");
            return
        }

        if (this.state.password !== this.state.repeatPassword) {
            pushErrorMessage("passwords don't match");
            return 
        }

        resetErrorMessage();
        signupUser(_.pick(this.state, "name", "email", "password"));
    }

    render() {
        const { isAuthenticated, isFetching } = this.props;

        if (isAuthenticated === true) {
            return (
                <Redirect to="/" />
            );
        } else {
            return (
                <div>
                    <h5>Create new account</h5>
                    <form onSubmit={this.handleSubmit}>
                        <input name="name" type="text" disabled={isFetching} placeholder="Name" value={this.state.name} onChange={this.handleInputChange} />
                        <br />
                        <input name="email" type="email" disabled={isFetching} placeholder="Email" value={this.state.email} onChange={this.handleInputChange} />
                        <br />
                        <input name="password" type="password" disabled={isFetching} placeholder="Password" value={this.state.password} onChange={this.handleInputChange} />
                        <br />
                        <input name="repeatPassword" type="password" disabled={isFetching} placeholder="Repeat password" value={this.state.repeatPassword} onChange={this.handleInputChange} />
                        <br />
                        <input type="submit" value="Sign Up" disabled={isFetching} />
                    </form>  
                </div>
            );
        }
    }
}

const mapStateToProps = (state, ownProps) => ({
    isFetching: state.auth.isFetching,
    isAuthenticated: state.auth.isAuthenticated,
});

const mapActionsToProps = {
    pushErrorMessage,
    resetErrorMessage,
    signupUser,
};

export default withRouter(connect(mapStateToProps, mapActionsToProps)(SignupPage));
