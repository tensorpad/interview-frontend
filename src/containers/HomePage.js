import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter, Redirect } from 'react-router-dom';

class HomePage extends Component {
    static propTypes = {
        isAuthenticated: PropTypes.bool.isRequired,
        user: PropTypes.shape({
            name: PropTypes.string.isRequired,
            email: PropTypes.string.isRequired,
            created: PropTypes.string.isRequired,
        }),
    }
    
    render() {
        const { isAuthenticated, user } = this.props;
        if (!isAuthenticated) {
            return <Redirect to="/login" />
        } else {
            return (
                <div>
                    <b>Welcome to Tensorpad Dashboard, { user ? user.name : null }!</b>
                </div>
            );
        }
    }
}

const mapStateToProps = (state, ownProps) => ({
    isAuthenticated: state.auth.isAuthenticated,
    user: state.user,
});

const mapActionsToProps = {
    // stub
}

export default withRouter(connect(mapStateToProps, mapActionsToProps)(HomePage));
