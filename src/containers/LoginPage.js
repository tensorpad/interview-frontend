import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter, Redirect } from 'react-router-dom';
import { pushErrorMessage, resetErrorMessage } from '../actions';

import { loginUser } from '../actions';

class LoginPage extends Component {
    static propTypes = {
        history: PropTypes.shape({
            push: PropTypes.func.isRequired,
        }),
        isFetching: PropTypes.bool.isRequired,
        isAuthenticated: PropTypes.bool.isRequired,
        loginUser: PropTypes.func.isRequired,
        pushErrorMessage: PropTypes.func.isRequired,
        resetErrorMessage: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);
        
        this.state = {
            email: '',
            password: '',
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        const creds = {
            email: this.state.email.trim(),
            password: this.state.password.trim()
        };

        this.props.loginUser(creds);
        event.preventDefault();
    }

    renderLoginForm() {
        const { isFetching } = this.props;
        return (
            <div>
                <h5>Login to dashboard</h5>
                <form onSubmit={this.handleSubmit}>
                    <input disabled={isFetching} name="email" type="email" placeholder="Email" value={this.state.email} onChange={this.handleInputChange} />
                    <br />
                    <input disabled={isFetching} name="password" type="password" placeholder="Password" value={this.state.password} onChange={this.handleInputChange} />
                    <br />
                    <input disabled={isFetching} type="submit" value="Log in" />
                </form>
            </div>
        );
    }

    render() {
        const { isAuthenticated } = this.props;
        if (isAuthenticated) {
            return (
                <Redirect to="/" />
            );
        } else {
            return this.renderLoginForm();
        }
    }
}

const mapStateToProps = (state, ownProps) => ({
    isFetching: state.auth.isFetching,
    isAuthenticated: state.auth.isAuthenticated,
});

export default withRouter(connect(mapStateToProps, {
    loginUser,
    pushErrorMessage,
    resetErrorMessage,
})(LoginPage));
