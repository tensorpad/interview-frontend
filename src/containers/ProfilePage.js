import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter, Redirect } from 'react-router-dom';

class ProfilePage extends Component {
    static propTypes = {
        isAuthenticated: PropTypes.bool.isRequired,
        user: PropTypes.shape({
            name: PropTypes.string.isRequired,
            email: PropTypes.string.isRequired,
            created: PropTypes.string.isRequired,
        }),
    }

    render() {
        if (this.props.isAuthenticated === false) {
            return <Redirect to="/login" />
        }
        
        const { user } = this.props;

        if (!user) {
            return <p>User cannot be shown</p>;
        }

        return (
            <div>
                <p>Name: { user.name }</p>
                <p>Email: { user.email }</p>
                <p>Created: { user.created }</p>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    isAuthenticated: state.auth.isAuthenticated,
    user: state.user,
});

export default withRouter(connect(mapStateToProps, {
    // stub
})(ProfilePage));
