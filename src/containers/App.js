import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { resetErrorMessage, logoutUser, fetchCurrentUserInfo } from '../actions';

import NavBar from '../components/NavBar';

class App extends Component {
    static propTypes = {
        errorMessage: PropTypes.string,
        resetErrorMessage: PropTypes.func.isRequired,
        logoutUser: PropTypes.func.isRequired,
        fetchCurrentUserInfo: PropTypes.func.isRequired,
        // Injected by React Router
        children: PropTypes.node,
    };

    constructor(props) {
        super(props);

        this.handleDissmissClick = this.handleDissmissClick.bind(this);
    }

    componentWillMount() {
        this.bootstrap();
    }

    bootstrap() {
        const { auth, fetchCurrentUserInfo } = this.props;
        if (auth.isAuthenticated) {
            fetchCurrentUserInfo();
        }
    }

    isBootstrapped() {
        const { auth, user } = this.props; 
        if (!auth.isAuthenticated) { return true; }
        return !!user 
    }

    handleDissmissClick = e => {
        this.props.resetErrorMessage();
        e.preventDefault();
    }

    renderErrorMessage() {
        const { errorMessage } = this.props;
        
        if (!errorMessage) {
            return null;
        }

        return (
            <div>
                <p><b>ERROR: </b>{ errorMessage }</p>
                <button onClick={this.handleDissmissClick}>Dismiss error</button>
            </div>
        );
    }

    renderLoadingScreen() {
        return (
            <h2>Loading...</h2>
        );
    }

    render() {
        const { children, logoutUser, auth } = this.props;
        
        if (this.isBootstrapped() === false) {
            return this.renderLoadingScreen();
        }

        return (
            <div>
                <NavBar isLoggedIn={auth.isAuthenticated} logoutUser={logoutUser} />
                { this.renderErrorMessage() }
                { children }
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    errorMessage: state.errorMessage,
    auth: state.auth,
    user: state.user,
});

const mapActionsToProps = {
    resetErrorMessage,
    logoutUser,
    fetchCurrentUserInfo,
};

export default withRouter(connect(mapStateToProps, mapActionsToProps)(App));
