import React from 'react';
import PropTypes from 'prop-types';

import { Provider } from 'react-redux';
import { Route } from 'react-router-dom';

import App from './App';
import HomePage from './HomePage';
import SignupPage from './SignupPage';
import LoginPage from './LoginPage';
import ProfilePage from './ProfilePage';

const Router = ({ store }) => (
    <Provider store={store}>
        <div>
            <Route path="/" component={App} />
            <Route exact={true} path="/" component={HomePage} />
            <Route path="/signup" component={SignupPage} />
            <Route path="/login" component={LoginPage} />
            <Route path="/profile" component={ProfilePage} />
        </div>
    </Provider>
);

Router.propTypes = {
    store: PropTypes.object.isRequired,
};

export default Router;
