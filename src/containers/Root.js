import React from 'react';
import PropTypes from 'prop-types';

import Router from './Router'

const Root = ({ store }) => (
    <Router store={store} />
);

Root.propTypes = {
    store: PropTypes.object.isRequired,
};

export default Root;
