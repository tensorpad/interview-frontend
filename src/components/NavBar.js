import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class NavBar extends Component {
    static propTypes = {
        isLoggedIn: PropTypes.bool.isRequired,
        logoutUser: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.logout = this.logout.bind(this);

        this.state = {
            isOpen: false
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    logout() {
        this.props.logoutUser();
    }

    renderUserMenu() {
        // Logout link + logic will go here
        return (
            <div>
                <Link to="/profile">Profile</Link>
                &nbsp;
                <button onClick={this.logout}>Logout</button>
            </div>
        )
    }

    renderAnonymousMenu() {
        return (
            <div>
                <Link to="/login">Log in</Link>
                &nbsp;
                <Link to="/signup"><b>Sign Up</b></Link>
            </div>
        );
    }

    render () {
        const { isLoggedIn } = this.props;
        return (
            <nav>
                <h2><Link to="/">Tensorpad Dashboard</Link></h2>
                { isLoggedIn ? this.renderUserMenu() : this.renderAnonymousMenu() }
            </nav>
        );
    }

}

export default NavBar;
