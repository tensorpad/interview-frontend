export const Endpoints = {
    Signup: "/auth/signup",
    Login: "/auth/login",
    Logout: "/auth/logout",

    // Protected
    Profile: "/api/v1/me",
};

// Just to demonstrate the interface of an object that implements API.

/*eslint-disable */
const EmptyAPI =
{
    Login: (creds) => {
        return new Promise((resolve, reject) => {
            reject();
        });
    },

    Logout: () => {
        return new Promise((resolve, reject) => {
            reject();
        });
    },

    Signup: (signupData) => {
        return new Promise((resolve, reject) => {
            reject();
        });
    },

    Profile: (creds) => {
        return new Promise((resolve, reject) => {
            reject();
        });
    },
}
/*eslint-enable */
