import { Endpoints } from './api';

export const makeFetchApi = (baseUrl) => {
    return {
        Login: (creds) => {
            return new Promise((resolve, reject) => {
                let config = {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(creds),
                    credentials: 'include',
                };

                return fetch(baseUrl+Endpoints.Login, config)
                    .then(response => {
                        if (!response.ok) {
                            return response.json()
                                .then((reply) => reject(new Error(reply['details'] || response.statusText)));
                        } else {
                            // cookie has been set
                            return resolve();
                        }
                    })
            })
        },
    
        Logout: () => {
            return new Promise((resolve, reject) => {
                let config = { method: "GET", credentials: "include" };
                return fetch(baseUrl+Endpoints.Logout, config)
                    .then(response => {
                        if (!response.ok) {
                            return response.json()
                                .then((reply) => reject(new Error(reply['details'] || response.statusText)));
                        } else {
                            return resolve();
                        }
                    })
            })
        },
    
        Signup: (signupData) => {
            return new Promise((resolve, reject) => {
                let config = {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(signupData),
                };
                
                return fetch(baseUrl+Endpoints.Signup, config)
                    .then((response) => {
                        if (!response.ok || response.status !== 201) {
                            return response.json()
                                .then((reply) => reject(new Error(reply['details'] || response.statusText)));
                        } else {
                            return resolve();
                        }
                    })
            })
        },
    
        Profile: (creds) => {
            return new Promise((resolve, reject) => {
                let config = {
                    method: "GET",
                    credentials: 'include',
                };

                return fetch(baseUrl+Endpoints.Profile, config)
                    .then(response => {
                        if (!response.ok) {
                            return response.json()
                                .then((reply) => reject(new Error(reply['details'] || response.statusText)));
                        }
                        return response.json()
                            .then((user) => {
                                if (!user) {
                                    return reject(new Error("empty user received"));
                                }

                                return resolve(user);
                            })
                            .catch((err) => {
                                return reject(err);
                            })
                    })
            })
        },
    }
}
