import { makeFetchApi } from '../api/fetch';
var _ = require('lodash');

const API = makeFetchApi("https://api.tensorpad.com");

// Action types

export const APP_BOOTSTRAP_COMPLETE = 'APP_BOOTSTRAP_COMPLETE';
export const PUSH_ERROR_MESSAGE = 'PUSH_ERROR_MESSAGE';
export const RESET_ERROR_MESSAGE = 'RESET_ERROR_MESSAGE';
export const SIGNUP_REQUEST = 'SIGNUP_REQUEST';
export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS';
export const SIGNUP_FAILURE = 'SIGNUP_FAILURE';
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const LOGOUT_FAILURE = 'LOGOUT_FAILURE';
export const CUR_USER_INFO_REQUEST = "CUR_USER_INFO_REQUEST";
export const CUR_USER_INFO_SUCCESS = "CUR_USER_INFO_SUCCESS";
export const CUR_USER_INFO_FAILURE = "CUR_USER_INFO_FAILURE";

// Action functions

export const pushErrorMessage = (error) => ({
    type: PUSH_ERROR_MESSAGE,
    error
});

export const resetErrorMessage = () => ({
    type: RESET_ERROR_MESSAGE
});

export const loginUser = (creds) => {
    var filteredCreds = _.pick(creds, "email", "password");
    return dispatch => {
        dispatch(requestLogin(creds))
        return API.Login(filteredCreds)
            .then(() => {
                localStorage.setItem('loggedIn', 'true');
                dispatch(loginSuccess());
                window.location.href = "/";
            })
            .catch((err) => {
                dispatch(loginError());
                dispatch(pushErrorMessage(err.message));  
            })
    }
}

export const signupUser = (data) => {
    var dataView = _.pick(data, "name", "email", "password");
    return (dispatch) => {
        dispatch(requestSignup(dataView));
        return API.Signup(dataView)
            .then(() => {
                dispatch(signupSuccess());
                window.location.href = "/login"
            })
            .catch((err) => {
                dispatch(signupError());
                dispatch(pushErrorMessage(err.message));
                console.error(err);
            })
    }
}

export const logoutUser = () => {
    return dispatch => {
        dispatch(requestLogout());
        return API.Logout()
            .then(() => {
                localStorage.removeItem('loggedIn');
                dispatch(receiveLogout());
                window.location.href = "/login"
            })
            .catch((err) => {
                dispatch(logoutError());
                dispatch(pushErrorMessage(err.message));
            })
    }
}

export const fetchCurrentUserInfo = () => {
    return (dispatch) => {
        dispatch(requestCurUserInfo());
        return API.Profile()
            .then((user) => {
                dispatch(getCurUserInfoSuccess(user));
            })
            .catch((err) => {
                dispatch(getCurUserInfoError());
                dispatch(pushErrorMessage(err.message));
            })
    }
}

// Action object builders

function requestSignup(data) {
    return {
        type: SIGNUP_REQUEST,
        isFetching: true,
        data
    }
}

function signupSuccess() {
    return {
        type: SIGNUP_SUCCESS,
        isFetching: false,
    }
}

function signupError() {
    return {
        type: SIGNUP_FAILURE,
        isFetching: false,
    }
}

function requestLogin(creds) {
    return {
        type: LOGIN_REQUEST,
        isFetching: true,
        isAuthenticated: false,
        creds
    }
}

function loginSuccess() {
    return {
        type: LOGIN_SUCCESS,
        isFetching: false,
        isAuthenticated: true,
    }
}

function loginError() {
    return {
        type: LOGIN_FAILURE,
        isFetching: false,
        isAuthenticated: false,
    }
}

function requestLogout() {
    return {
        type: LOGOUT_REQUEST,
        isFetching: true,
        isAuthenticated: true
    }
}

function receiveLogout() {
    return {
        type: LOGOUT_SUCCESS,
        isFetching: false,
        isAuthenticated: false
    }
}

function logoutError() {
    return {
        type: LOGOUT_FAILURE,
        isFetching: false,
        isAuthenticated: false,
    }
}

function requestCurUserInfo() {
    return {
        type: CUR_USER_INFO_REQUEST
    }
}

function getCurUserInfoSuccess(userInfo) {
    return {
        type: CUR_USER_INFO_SUCCESS,
        userInfo
    }
}

function getCurUserInfoError() {
    return {
        type: CUR_USER_INFO_FAILURE,
    }
}
