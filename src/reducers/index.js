import * as ActionTypes from '../actions';
import { combineReducers } from 'redux';

const errorMessage = (state = null, action) => {
    switch (action.type) {
        case ActionTypes.PUSH_ERROR_MESSAGE:
            return action.error || 'Unknown error';
        case ActionTypes.RESET_ERROR_MESSAGE:
            return null;
        default:
            return state;
    }
}

// Auth state storing only isAuthenticated bool, the tokens are 
// stored in the cookies.
const auth = (state = {
    isFetching: false,
    isAuthenticated: localStorage.getItem('loggedIn') != null,
}, action) => {
    switch (action.type) {
        case ActionTypes.LOGIN_REQUEST:
            return Object.assign({}, state, {
                isFetching: true,
                isAuthenticated: false,
                user: action.creds,
            });
        case ActionTypes.LOGIN_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                isAuthenticated: true,
            });
        case ActionTypes.LOGIN_FAILURE:
            return Object.assign({}, state, {
                isFetching: false,
                isAuthenticated: false,
            });
        case ActionTypes.LOGOUT_SUCCESS:
            return Object.assign({}, state, {
                isFetching: true,
                isAuthenticated: false,
            });
        default:
            return state;
    }
}

const user = (state = null, action) => {
    switch (action.type) {
        case ActionTypes.CUR_USER_INFO_SUCCESS:
            return Object.assign({}, action.userInfo);

        case ActionTypes.LOGOUT_SUCCESS:
            return null;

        default:
            return state;
    }
}

const rootReducer = combineReducers({
    auth,
    user,
    errorMessage,
});

export default rootReducer;
